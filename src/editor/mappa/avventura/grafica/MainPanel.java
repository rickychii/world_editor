package editor.mappa.avventura.grafica;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class MainPanel extends JLayeredPane implements Runnable {
    
    static final long serialVersionUID = 10;

    private final int ColoreCLS=0x9A9ADD;
    private final int ColoreBottoniOggetto = 0xFFAAAA;
    private final int ColoreBottoniMappa = 0xA9A9FF;
    private final int ColoreToggleAnima = 0x8A8A8A;
    
    public static int widthMappa;
    public static int heightMappa;
    
    JPanel jpanelImage;
    JTextArea mouseX;
    JTextArea mouseY;
    JTextArea textScriptMappa;
    
    //SE TRUE ANIMAZIONE IN CORSO
    public static boolean stoAnimando;
    
    //LABEL COMPILATO
    JLabel compilato=new JLabel();
    
    //RIGHELLI
    JPanel righelloInferiore;
    JPanel righelloSinistro;
    
    //STRUMENTO LINEA DEPRECATO.
    //public static boolean strumentolinea;
    
    //MAPPA ATTUALE
    public static MappaAttuale mappa;
    
    //Oggetti Attuali
    private static JComboBox<String> jcomboOggetti;
    
    //MAI USATO, DEPRECATO.
    public JButton linea;
    public JButton nuovoOggetto;
    public JButton caricaSfondo;
    public JButton caricaImmagine;
    public JButton dettagliOggetto;
    public JButton dettagliCollisione;
    public JButton dettagliAnimazione;
    public JButton SalvaMappa;
    
    public JButton genera;
    public JButton caricaMappa;
    public JButton reset;
            
    public JToggleButton buttonStartAnimazione;
    
    //MENU
    private JPanel menu;
    private JLabel dimensioni;
    
    public MainPanel(int width, int height, int widthCanvas)
    {
        widthMappa = widthCanvas;
        heightMappa = EditorMappaAvventuraGrafica.height;
        
        PannelloImmagine jpanelImage;
        jpanelImage = new PannelloImmagine(widthCanvas,480);
        textScriptMappa = new JTextArea();
        
        if(dimensioni != null)
        {
            menu.remove(dimensioni);
            dimensioni = null;
        }
        dimensioni = new JLabel(widthCanvas + "px  "+"  480px");
        dimensioni.setLocation(0,0);
        
        caricaImmagine = new JButton("Carica Immagine");
        caricaImmagine.setBackground(new Color(ColoreBottoniOggetto));
        caricaImmagine.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        stoAnimando = false;
        
        caricaImmagine.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser();
                JButton jb = (JButton)(e.getSource());
                JPanel jp = (JPanel)(jb.getParent());
                fc.addChoosableFileFilter(new FileNameExtensionFilter("Image Files", "jpg", "png", "tif"));
                fc.setAcceptAllFileFilterUsed(false);
                int returnVal = fc.showOpenDialog(jp);
                
                //SE SELEZIONA OK SU AGGIUNGI FILE
                //AGGIUNGO L'IMMAGINE ALL'OGGETTO..
                if (returnVal == JFileChooser.APPROVE_OPTION) 
                {
                    File file = fc.getSelectedFile();
                    BufferedImage img = null;
                    try
                    {
                        img = ImageIO.read(file);
                    }
                    catch(IOException ex)
                    {
                        JOptionPane.showMessageDialog(null, "C'è stato un problema con la lettura dell'immagine","Ric.C", JOptionPane.OK_OPTION);
                        return;
                    }
                    //HO L'IMMAGINE IN MANO: L'aggiungo all'oggetto.
                    //1. prendo l'oggetto attuale.
                    String id_oggetto = getSelectedObject();
                    
                    //2. prendo l'indice attuale
                    int index = mappa.getIndex(id_oggetto);
                    
                    //3. aggiungo l'immagine all'oggetto
                    Oggetto target = (Oggetto)(mappa.listaOggetti.get(index));
                    target.setImmagine(img, file.getName());
                    
                    //4. ELIMINO EVENTUALI COLLISIONI
                    target.setCollisione("null");
                }
                else 
                {
                }
        }});
        
        caricaSfondo = new JButton("Carica Sfondo");
        caricaSfondo.setBackground(Color.yellow);
        caricaSfondo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JFileChooser fc = new JFileChooser();
                JButton jb = (JButton)(ae.getSource());
                JPanel jp = (JPanel)(jb.getParent());
                fc.addChoosableFileFilter(new FileNameExtensionFilter("Image Files", "jpg", "png", "tif"));
                fc.setAcceptAllFileFilterUsed(false);
                int returnVal = fc.showOpenDialog(jp);
                
                //SE SELEZIONA OK SU AGGIUNGI FILE
                //AGGIUNGO L'IMMAGINE ALL'OGGETTO..
                if (returnVal == JFileChooser.APPROVE_OPTION) 
                {
                    File file = fc.getSelectedFile();
                    BufferedImage img = null;
                    try
                    {
                        img = ImageIO.read(file);
                    }
                    catch(IOException ex)
                    {
                        JOptionPane.showMessageDialog(null, "C'è stato un problema con la lettura dell'immagine","Ric.C", JOptionPane.OK_OPTION);
                        return;
                    }
                    //HO L'IMMAGINE IN MANO: IMPOSTO LO SFONDO EVENTUALMENTE STRETCHANDOLO.
                    int widthMAPPA = MainPanel.widthMappa;
                    int heightMAPPA = MainPanel.heightMappa;
                    
                    //System.out.println(widthMAPPA+" "+heightMAPPA);
                    
                    MainPanel.mappa.sfondo = img.getScaledInstance(widthMAPPA,heightMAPPA, BufferedImage.TYPE_INT_RGB);
                    MainPanel.mappa.pathsfondo = EditorMappaAvventuraGrafica.percorsoImmagini + file.getName();
                }
                else 
                {
                }
            }
        });
        
        /*
        LINEA DEPRECATA
        linea = new JButton("Linea");
        linea.setBackground(null);
        linea.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JButton jb = (JButton)(ae.getSource());
                strumentolinea=!strumentolinea;
                if(strumentolinea)
                {
                    jb.setBackground(Color.PINK);
                }
                else
                {
                    jb.setBackground(null);
                }
            }   
        });
        linea.setAlignmentX(Component.CENTER_ALIGNMENT);*/
        
        reset = new JButton("Nuova Mappa");
        reset.setBackground(new Color(ColoreBottoniMappa));
        
        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int dialogButton = JOptionPane.showConfirmDialog 
                (null, "Attenzione, cliccando su 'SI' perderai tutte le modifiche. Continuare?","Ric.C", JOptionPane.YES_NO_OPTION);
                if(dialogButton==JOptionPane.YES_OPTION)
                {
                    FinestrellaDimensioni nuovo = new FinestrellaDimensioni();
                    nuovo.setVisible(true);
                    
                    JFrame j = EditorMappaAvventuraGrafica.MainWindow;
                    j.dispose();
                    j = null; 
                }
            }
        });
        
        reset.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        genera = new JButton("Genera Script");
        genera.setBackground(new Color(ColoreBottoniMappa));
        genera.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        genera.addActionListener(new ActionListener() {
 
            @Override
            public void actionPerformed(ActionEvent e)
            {
                JButton jb = (JButton)e.getSource();
                MainPanel mp = (MainPanel)jb.getParent().getParent();
                mp.textScriptMappa.setText(mappa.getScript());
            }
        });  
        
        caricaMappa = new JButton("Carica Mappa");
        caricaMappa.setBackground(new Color(ColoreBottoniMappa));
        caricaMappa.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                JButton jb = (JButton)(ae.getSource());
                MainPanel jp = (MainPanel)(jb.getParent().getParent());
                
                JFileChooser fc = new JFileChooser();
                fc.setDialogTitle("Caricamento mappa");
                fc.addChoosableFileFilter(new FileNameExtensionFilter("XML Files","xml"));
                fc.setAcceptAllFileFilterUsed(false);
                
                int returnVal = fc.showOpenDialog(jp);
                
                //SE SELEZIONA OK SU AGGIUNGI FILE
                //PRENDO IL CONTENUTO DEL FILE E VADO A CREARE UNA NUOVA MAPPA
                if (returnVal == JFileChooser.APPROVE_OPTION) 
                {
                    File file = fc.getSelectedFile();
                    EditorMappaAvventuraGrafica.CaricaMappa(file);
                }
                else 
                {
                }
            }
        });
        
        SalvaMappa = new JButton("Salva Mappa");
        SalvaMappa.setBackground(new Color(ColoreBottoniMappa));
        SalvaMappa.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                JButton sender = (JButton) ae.getSource();
                MainPanel panel = (MainPanel)sender.getParent().getParent();
                
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Salvataggio mappa");
                fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("XML Files", "xml"));
                fileChooser.setAcceptAllFileFilterUsed(false);
                
                
                int userSelection = fileChooser.showSaveDialog(panel);
                
                if (userSelection == JFileChooser.APPROVE_OPTION) 
                {
                    File fileToSave = fileChooser.getSelectedFile();
                    String filePath = fileToSave.getPath();
                    
                    if(!filePath.toLowerCase().endsWith(".xml"))
                    {
                        filePath += ".xml";
                        fileToSave = new File(filePath);
                    }
                    
                    
                    if(!fileToSave.exists())
                    {
                        EditorMappaAvventuraGrafica.SalvaMappa(fileToSave);
                    }
                    else
                    {
                        int dialogButton = JOptionPane.showConfirmDialog(null, "Il file esiste gia. Lo sostituisco?","Ric.C", JOptionPane.YES_NO_OPTION);
                        if(dialogButton == JOptionPane.YES_OPTION)
                        {
                            EditorMappaAvventuraGrafica.SalvaMappa(fileToSave);
                        }
                        else
                        {
                            //CLICCO IN AUTOMATICO IL PULSANTE DI SALVATAGGIO NEL CASO SIA PREMUTO NO ALLA DOMANDA SOSTITUISCO?
                            panel.SalvaMappa.doClick();
                        }
                    }
                }
            }
        });
        
        //AGGIUNGO UN NUOVO OGGETTO
        nuovoOggetto = new JButton("Nuovo Oggetto");
        nuovoOggetto.setBackground(new Color(ColoreBottoniOggetto));
        nuovoOggetto.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                JButton jb = (JButton)ae.getSource();
                JFrame jf= (JFrame)(jb.getParent().getParent().getParent().getParent().getParent().getParent());
                FinestrellaNuovoOggetto fin = new FinestrellaNuovoOggetto(jf);
            }
        });
        //VEDO I DETTAGLI DI UN OGGETTO
        dettagliOggetto = new JButton("Dettagli Oggetto");
        dettagliOggetto.setBackground(new Color(ColoreBottoniOggetto));
        dettagliOggetto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JButton jb = (JButton)ae.getSource();
                
                JFrame jf= (JFrame)(jb.getParent().getParent().getParent().getParent().getParent().getParent());
                
                String idoggettocorrente = (String)(jcomboOggetti.getSelectedItem());
                
                Oggetto oggettoAttuale = MainPanel.mappa.getOggetto(idoggettocorrente);
                //COSTRUISCO FINESTRELLA CON OGGETTOATTUALE E JFRAME (PARENT)
                FinestrellaDettagliOggetto fin = new FinestrellaDettagliOggetto(oggettoAttuale, jf);
            }
        });
        
        dettagliCollisione = new JButton("Dettagli Collisione");
        dettagliCollisione.setBackground(new Color(ColoreBottoniOggetto));
        dettagliCollisione.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                JButton jb = (JButton)ae.getSource();
                
                JFrame jf= (JFrame)(jb.getParent().getParent().getParent().getParent().getParent().getParent());
                
                String idoggettocorrente = (String)(jcomboOggetti.getSelectedItem());
                
                Oggetto oggettoAttuale = MainPanel.mappa.getOggetto(idoggettocorrente);
                //COSTRUISCO FINESTRELLA CON OGGETTOATTUALE E JFRAME (PARENT)
                FinestrellaDettagliCollisioni fin = new FinestrellaDettagliCollisioni(oggettoAttuale, jf);
            }
        });
        
        dettagliAnimazione = new JButton("Dettagli Animazioni");
        dettagliAnimazione.setBackground(new Color(ColoreBottoniOggetto));
        dettagliAnimazione.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                String idoggettocorrente = (String)(jcomboOggetti.getSelectedItem());
                Oggetto ogg = MainPanel.mappa.getOggetto(idoggettocorrente);
                if(ogg.getImmagine().img == null)
                {
                    JOptionPane.showMessageDialog(null, "L'oggetto \"" + idoggettocorrente  + "\" non ha immagini associate","Ric.C", JOptionPane.OK_OPTION);
                }
                else if(ogg.getImmagine().isAnimated == false)
                {
                    JOptionPane.showMessageDialog(null, "L'oggetto \"" + idoggettocorrente  + "\" è settato 'animated' = false","Ric.C", JOptionPane.OK_OPTION);
                }
                else
                {
                    JButton jb = (JButton)ae.getSource();
                    JFrame jf = (JFrame)(jb.getParent().getParent().getParent().getParent().getParent().getParent());
                    
                    FinestrellaDettagliAnimazioni fin = new FinestrellaDettagliAnimazioni(ogg, jf);
                }
            }
        });
        
        buttonStartAnimazione = new JToggleButton("ANIMATE!");
        buttonStartAnimazione.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JToggleButton tb = (JToggleButton)(ae.getSource());
                MainPanel pan = (MainPanel)(tb.getParent().getParent());
                stoAnimando = tb.isSelected();
                if(stoAnimando)
                {
                    pan.StartAnimation();
                }
                else
                {
                    pan.StopAnimation();
                }
            }
        });
        buttonStartAnimazione.setBackground(new Color(ColoreToggleAnima));
        buttonStartAnimazione.setForeground(Color.blue);
        
        if(menu != null)
            this.remove(menu);
        
        menu = null;
        menu = new JPanel();
        menu.setSize(300,360);
        menu.setBackground(new Color(0xAAAAFF));
        
        textScriptMappa.setEditable(true);
        textScriptMappa.setFocusable(true);
        
        JScrollPane scrolltxt = new JScrollPane(textScriptMappa);
        scrolltxt.setSize(1000,185);
        scrolltxt.setLocation(5,550);
        scrolltxt.setEnabled(true);
               
        mouseX = new JTextArea();
        mouseX.setBackground(new Color(0xAAAAAA));
        mouseX.setSize(90,30);
        mouseX.setEditable(false);
        mouseX.setLocation(710, 400);
        mouseX.setFocusable(false);
        
        mouseY = new JTextArea();
        mouseY.setBackground(new Color(0xAAAAAA));
        mouseY.setSize(90,30);
        mouseY.setEditable(false);
        mouseY.setLocation(810, 400);
        mouseY.setFocusable(false);
        
        Font font = new Font("Verdana", Font.TYPE1_FONT, 20);
        
        mouseX.setFont(font);
        mouseX.setForeground(Color.BLUE);
        mouseY.setFont(font);
        mouseY.setForeground(Color.BLUE);
        
        jcomboOggetti= new JComboBox<>();
        jcomboOggetti.setBackground(new Color(ColoreBottoniOggetto));
        jcomboOggetti.addItem("All");
        jcomboOggetti.addItemListener(new ItemListener(){
            @Override
             public void itemStateChanged(ItemEvent arg) {
                JComboBox sender = (JComboBox)(arg.getSource());
                MainPanel mainpanel = (MainPanel)(sender.getParent().getParent());
                //JOptionPane.showMessageDialog(sender.getParent(),sender.getSelectedItem());
                
                if(sender.getSelectedItem()!="All")
                {
                    enableBottoniOggetto();
                }
                else
                {
                    disableBottoniOggetto();
                }
                
                //AZZERO ELEMENTI SELEZIONATI DALL'IMMAGINE
                PannelloImmagine.azzeraOggettiSelezionati();
                
                //AZZERO VERTICI COLLISIONE SELEZIONATI
                for(int i=0; i<MainPanel.mappa.getNumeroOggetti(); ++i)
                {
                    BaseCollisione tmp = (BaseCollisione)(MainPanel.mappa.popOggetto(i).getObjectCollisione());
                    if(tmp != null)
                        tmp.AzzeraSelezionePunti();
                }
            }
        });
        
        menu.setLocation(width-20-300,18);
        
        GridBagLayout gbl= new GridBagLayout();
        menu.setLayout(gbl);
        
        GridBagConstraints lim = new GridBagConstraints();
        GridBagConstraints separatorConstraint = new GridBagConstraints();
        
        lim.fill = GridBagConstraints.NORTH;
        
        lim.gridx=0;
        lim.gridy=0;
        lim.ipadx=180;
        menu.add(caricaSfondo, lim);
        
        lim.gridx=0;
        lim.gridy=10;
        lim.ipadx=200;
        JLabel label1=new JLabel("Oggetto"); 
        menu.add(label1,lim);
        
        lim.gridx=0;
        lim.gridy=20;
        lim.ipadx=200;
        menu.add(jcomboOggetti, lim);
        
        lim.gridx=0;
        lim.gridy=30;
        lim.ipadx=125;
        menu.add(nuovoOggetto, lim);
        
        /*
        separatorConstraint.weightx = 1.0;
        separatorConstraint.fill = GridBagConstraints.HORIZONTAL;
        separatorConstraint.gridwidth = GridBagConstraints.RELATIVE;
        separatorConstraint.gridx=0;
        separatorConstraint.gridy=3;
        menu.add(new JSeparator(JSeparator.HORIZONTAL), separatorConstraint);
        */
        
        lim.gridx=0;
        lim.gridy=40;
        lim.ipadx=117;
        menu.add(dettagliOggetto, lim);
        dettagliOggetto.setEnabled(false);
        
        lim.gridx=0;
        lim.gridy=50;
        lim.ipadx=107;
        menu.add(dettagliCollisione, lim);
        dettagliCollisione.setEnabled(false);
        
        lim.gridx=0;
        lim.gridy=60;
        lim.ipadx=100;
        dettagliAnimazione.setEnabled(false);
        menu.add(dettagliAnimazione, lim);
        
        
        lim.gridx=0;
        lim.gridy=70;
        lim.ipadx=112;
        caricaImmagine.setEnabled(false);
        menu.add(caricaImmagine, lim);
        

        /*
        lim.gridx=0;
        lim.gridy=6;
        lim.ipadx=176;
        linea.setEnabled(false);*/
        //26.05.2014: commento pulsante linea in favore di collisioni create ad-hoc.
        //menu.add(linea, lim);
        
        separatorConstraint.weightx = 1.0;
        separatorConstraint.fill = GridBagConstraints.HORIZONTAL;
        separatorConstraint.gridwidth = GridBagConstraints.REMAINDER;
        separatorConstraint.gridx=0;
        separatorConstraint.gridy=8;
        menu.add(new JSeparator(JSeparator.HORIZONTAL), separatorConstraint);
        
        lim.gridx=0;
        lim.gridy=80;
        lim.ipadx=130;
        menu.add(genera, lim);
        
        lim.gridx=0;
        lim.gridy=90;
        lim.ipadx=130;
        menu.add(caricaMappa, lim);
        
        
        lim.gridx=0;
        lim.gridy=100;
        lim.ipadx=136;
        menu.add(SalvaMappa, lim);
        
        lim.gridx=0;
        lim.gridy=110;
        lim.ipadx=132;
        menu.add(reset, lim);
        
        lim.gridx=0;
        lim.gridy=120;
        lim.ipadx=155;
        menu.add(buttonStartAnimazione, lim);
        
        lim.gridx=0;
        lim.gridy=130;
        menu.add(dimensioni, lim);

        this.repaint();
       
        //DEVO FARGLI ASCOLTARE LO SHIFT!!
        righelloInferiore = new JPanel(){
            @Override
            public void paint(Graphics grphcs) {
                grphcs.setColor(new Color(ColoreCLS));
                grphcs.fillRect(0,0,this.getWidth(),this.getHeight());
                grphcs.setColor(Color.BLUE);
                for(int i=20; i<this.getWidth(); i+=40)
                {
                    grphcs.drawString(String.valueOf(i-20 + PannelloImmagine.shiftX),i-3,this.getHeight()-6);
                }
            }   
        };
        
        //NON SERVE LO SHIFT
        righelloSinistro = new JPanel(){
            @Override
            public void paint(Graphics grphcs) {
                grphcs.setColor(new Color(ColoreCLS));
                grphcs.fillRect(0,0,this.getWidth(),this.getHeight());
                grphcs.setColor(Color.BLUE);
                for(int i=0; i <= editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.height; i+=40)
                {
                    grphcs.drawString(String.valueOf(i),0, i + 24);
                }
            }   
        };
        
        //strumentolinea = false;
        
        
        //height+40:
        //20 aggiunti sopra e 20 aggiunti sotto
        righelloSinistro.setSize(20,editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.height + 48);
        righelloSinistro.setLocation(4, 20);
        righelloSinistro.setVisible(true);
        righelloSinistro.setFocusable(false);
        
        //height+40:
        //20 aggiunti a destra e 20 aggiunti a sinistra
        righelloInferiore.setSize(editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.width + 50,20);
        righelloInferiore.setLocation(20, 528);
        righelloInferiore.setVisible(true);
        righelloInferiore.setFocusable(false);
        
        
        this.setLayout(null);
        jpanelImage.setLocation(23,20);
        this.add(menu);
        this.add(scrolltxt);
        this.add(mouseX);
        this.add(mouseY);
        
        
        this.add(righelloInferiore, 5, 10);
        this.add(righelloSinistro, 10, 10);
        
        jpanelImage.setLocation(40, 40);
        this.add(jpanelImage);
        
        //COMPILATO
        compilato.setText("V 1.0 Compilato il 18/12/2014");
        compilato.setForeground(Color.BLUE);
        compilato.setLocation(40,0);
        compilato.setSize(200,30);
        this.add(compilato);
        
        /*
        this.moveToBack(righelloSinistro);
        this.moveToBack(righelloInferiore);
        this.moveToFront(jpanelImage);
        */
        
        //THREAD PER ASCOLTARE POSIZIONE XY DEL MOUSE
        new Thread(this).start();
        
        //MAPPA ATTUALE
        mappa=new MappaAttuale();
    }
    
    @Override
    protected void paintComponent(Graphics g) 
    {
        g.setColor(new Color(ColoreCLS));
        g.fillRect(0,0,1024,768);
    }   

    @Override
    public void run() {
        while(true)
        {
            try
            {
                if(PannelloImmagine.mousedentro)
                {
                    mouseX.setText("X: " + String.valueOf(PannelloImmagine.get_X()));
                    mouseY.setText("Y: " + String.valueOf(PannelloImmagine.get_Y()));
                }
                else
                {
                    mouseX.setText("X: ");
                    mouseY.setText("Y: ");
                }
                //REPAINT DEL RIGHELLO INFERIORE
                righelloInferiore.repaint();
                
                Thread.sleep(20);
            }
            catch(Exception ex)
            {
                System.err.println(ex.toString());
            }
        }
    }
    //DROPDOWNLIST: AGGIUNGE UN ELEMENTO IN CODA (ritorna boolean)
    public static Boolean addDDLItem(String item)
    {
        try
        {
            jcomboOggetti.addItem(item);
            return true;
        }
        catch(Exception ex)
        {
            return false;
        }
    }
    //DROPDOWNLIST: RITORNA L'OGGETTO SELEZIONATO
    public static String getSelectedObject()
    {
        return (String)(jcomboOggetti.getSelectedItem());
    }
    
    //DATABIND SUL DROPDOWN....
    public static void databindJcombo(String iddaselezionare)
    {
        //System.out.println(iddaselezionare);
        
        jcomboOggetti.removeAllItems();
        jcomboOggetti.addItem("All");
        for(int i=0; i<mappa.listaOggetti.size(); ++i)
            jcomboOggetti.addItem(mappa.listaOggetti.get(i).id);
        
        jcomboOggetti.setSelectedItem(iddaselezionare);
    }
    
    //--------------------------------------------------------------------BLOCCO MENU E BOTTONI---------------------------------------------------
        //METODI CHE MI SERVONO PERCHE' TUTTI I BOTTONI VENGONO DISABILITATI
    //NEL MOMENTO IN CUI PARTE L'ANIMAZIONE.
    //ATTIVO IL MENU MA SE SERVE DISATTIVO I PULSANTI RELATIVI ALL'OGGETTO.
    public void enableMenu()
    {
        jcomboOggetti.setEnabled(true);
        
        caricaSfondo.setEnabled(true);
        nuovoOggetto.setEnabled(true);
        genera.setEnabled(true);
        caricaMappa.setEnabled(true);
        SalvaMappa.setEnabled(true);
        reset.setEnabled(true);
        
        caricaImmagine.setEnabled(true);
        dettagliOggetto.setEnabled(true);
        dettagliCollisione.setEnabled(true);
        dettagliAnimazione.setEnabled(true);
        caricaImmagine.setEnabled(true);
        
        //LINEA DEPRECATO
        //linea.setEnabled(true);
        
        dettagliOggetto.setEnabled(true);
        dettagliCollisione.setEnabled(true);
        dettagliAnimazione.setEnabled(true);
        if(!(jcomboOggetti.getSelectedItem().toString().toLowerCase().equals("all")))
        {
            enableBottoniOggetto();
        }
        else
        {
            disableBottoniOggetto();
        }
    }
    
    //DISATTIVO TUTTO IL MENU
    public void disableMenu()
    {
        jcomboOggetti.setEnabled(false);
        
        caricaSfondo.setEnabled(false);
        nuovoOggetto.setEnabled(false);
        genera.setEnabled(false);
        caricaMappa.setEnabled(false);
        SalvaMappa.setEnabled(false);
        reset.setEnabled(false);
        
        caricaImmagine.setEnabled(false);
        dettagliOggetto.setEnabled(false);
        dettagliCollisione.setEnabled(false);
        dettagliAnimazione.setEnabled(false);
        caricaImmagine.setEnabled(false);
        
        //LINEA DEPRECATO
        //linea.setEnabled(false);
        
        dettagliOggetto.setEnabled(false);
        dettagliCollisione.setEnabled(false);
        dettagliAnimazione.setEnabled(false);
    }
    
    
    //ENABLE BOTTONI OGGETTO
    private void enableBottoniOggetto()
    {
        caricaImmagine.setEnabled(true);
        
        //LINEA DEPRECATO
        //linea.setEnabled(true);
        
        dettagliOggetto.setEnabled(true);
        dettagliCollisione.setEnabled(true);
        dettagliAnimazione.setEnabled(true);
    }
    
    //DISABLE BOTTONI OGGETTO
    private void disableBottoniOggetto()
    {
        caricaImmagine.setEnabled(false);
        
        //LINEA DEPRECATO
        //linea.setEnabled(false);
        
        dettagliOggetto.setEnabled(false);
        dettagliCollisione.setEnabled(false);
        dettagliAnimazione.setEnabled(false);
    }
    
    
    //---------------------------------------------------------------------GESTIONE ANIMAZIONI-----------------------------------------------------
    public void StartAnimation()
    {
        stoAnimando = true;
        disableMenu();
        
        for(int i=0; i<mappa.getNumeroOggetti(); ++i)
        {
            Oggetto tmp = mappa.listaOggetti.get(i);
            if(tmp.getImmagine().isAnimated)
            {
                new Thread(tmp.getImmagine()).start();
            }
        }
    }
    public void StopAnimation()
    {
        stoAnimando = false;
        enableMenu();
        
        //I THREAD SI INTERROMPONO DA SOLI.
    }
}