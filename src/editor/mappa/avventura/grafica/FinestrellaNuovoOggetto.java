package editor.mappa.avventura.grafica;

import java.awt.*;
import java.awt.event.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.*;

public class FinestrellaNuovoOggetto extends JDialog
{
    public JTextField tbID;
    public JTextField tbZInd;
    
    static final long serialVersionUID = 10;

    public FinestrellaNuovoOggetto(final JFrame sender)
    {
        
        this.setTitle("Nuovo Oggetto");
        this.setSize(300,120);
        this.setResizable(false);
  
        this.requestFocus();
        //CENTRO DELLO SCHERMO!
        this.setLocationRelativeTo(null);
        
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        this.setLayout(new GridLayout(3,2));
        
        //label descrizione
        JLabel labelID = new JLabel("ID");
        JLabel labelZIndex = new JLabel("Z-Index");
        
        //Textbox
        tbID = new JTextField();
        tbZInd = new JTextField();
        
        tbID.setDocument(new JTextFieldLimit(15));
        tbZInd.setDocument(new JTextFieldLimit(5));
        //Bottoni
        JButton OK =new JButton("OK");
        JButton Annulla = new JButton("Annulla");
        
        Annulla.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                JButton x = (JButton)(ae.getSource());
                JDialog w = (JDialog)(x.getParent().getParent().getParent().getParent());
                w.dispose();
            }
        });
        
        OK.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                //PRENDO LE ISTANZE CHE MI SERVONO
                JButton x = (JButton)(ae.getSource());
                FinestrellaNuovoOggetto w = (FinestrellaNuovoOggetto)(x.getParent().getParent().getParent().getParent());
                
                //SE IL TESTO è VUOTO ANNULLO CON UN ALERT
                if (w.tbID.getText().equals("") ||
                        w.tbZInd.getText().equals(""))
                {
                    JOptionPane.showMessageDialog(null, "Inserire id e zindex","Ric.C", JOptionPane.OK_OPTION);
                    return;
                }
                
                if(w.tbID.getText().toLowerCase().equals("all"))
                {
                    JOptionPane.showMessageDialog(null, "Nome Oggetto non valido.","Ric.C", JOptionPane.OK_OPTION);
                    return;
                }
                
                //VALORI DA PRENDERE PER INSERIRE UN NUOVO OGGETTO
                String id = ((JTextField)(w.tbID)).getText();
                int zind;
                
                //PROVO A CASTARE ZINDEX
                try
                {
                    zind = Integer.parseInt((w.tbZInd).getText());
                }
                catch(NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(null, "Inserire valori validi!","Ric.C", JOptionPane.OK_OPTION);
                    return;
                }
                
                //CERCO SE ESISTONO ALTRI OGGETTI CON LO STESSO ID...
                if(MainPanel.mappa.esistegiaOggetto(id))
                {
                    JOptionPane.showMessageDialog(null, "Impossibile inserire, esiste gia un altro oggeto con questo ID!","Ric.C", JOptionPane.OK_OPTION);
                    return;
                }
                
                //CASO FINALE:
                //PUSHO GLI OGGETTI E CHIUDO LA FINESTRA ATTUALE.
                MainPanel.mappa.pushOggetto(id, zind);
                MainPanel.addDDLItem(id);
                w.dispose();
                //System.out.println("Push: "+id+"  "+zind);
            }
        });
        
        this.add(labelID);
        this.add(labelZIndex);
        
        this.add(tbID);
        this.add(tbZInd);
        
        this.add(OK);
        this.add(Annulla);
        
        this.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent we) {
                sender.setEnabled(false);
            }

            @Override
            public void windowClosing(WindowEvent we) {
            }

            @Override
            public void windowClosed(WindowEvent we) {
                sender.setEnabled(true);
                sender.requestFocus();
            }

            @Override
            public void windowIconified(WindowEvent we) {
            }

            @Override
            public void windowDeiconified(WindowEvent we) {
            }

            @Override
            public void windowActivated(WindowEvent we) {
            }

            @Override
            public void windowDeactivated(WindowEvent we) {
            }
        });
        this.validate();
        this.setVisible(true);
    }
}