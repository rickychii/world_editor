package editor.mappa.avventura.grafica;

import java.util.*;


/*
WIKIPEDIA... :D
procedure bubbleSort( A : list of sortable items )
    n = length(A)
    repeat
       newn = 0
       for i = 1 to n-1 inclusive do
          if A[i-1] > A[i] then
             swap(A[i-1], A[i])
             newn = i
          end if
       end for
       n = newn
    until n = 0
end procedure
*/

//RITORNA VETTORE DI INDICI ORDINATI IN BASE Z-INDEX
public class BubbleSortapplicatoaLista 
{
    public static int[] getIndiciOrderBY(List<Oggetto> lista)
    {
        int[] indici = new int[lista.size()];
        
        //Inizializzo Lista
        for(int i=0;i<lista.size();++i)
            indici[i]=i;
        
        int n = lista.size();
        int newn = 0;
        do
        {
            newn = 0;
            for(int i=1; i<n; ++i)
            {
                if(lista.get(i-1).zindex > 
                    lista.get(i).zindex)
                {
                    //SCAMBIO OGGETTO
                    Oggetto temp = lista.get(i-1);
                    lista.set(i-1,lista.get(i));
                    lista.set(i, temp);

                    //SCAMBIO VETTORE DI ZINDEX
                    //indici[i] = lista.get(i).zindex;
                    //indici[i-1] = lista.get(i-1).zindex;
                    
                    //SCAMBIO VETTORE DI INDICI
                    indici[i] = i;
                    indici[i-1] = i-1;
                    
                    //AGGIORNO N
                    newn = i;
                }
            }
            n = newn;
        }
        while(n != 0);
        return indici;
    }
}